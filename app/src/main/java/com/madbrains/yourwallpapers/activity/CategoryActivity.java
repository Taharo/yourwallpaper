package com.madbrains.yourwallpapers.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;
import com.madbrains.yourwallpapers.R;
import com.madbrains.yourwallpapers.adapter.DataAdapter;
import com.madbrains.yourwallpapers.model.Model;
import com.madbrains.yourwallpapers.provider.Query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryActivity extends AppCompatActivity {

    private String title;
    private String category;

    private ProgressBar progressBar;
    private ImageView errorImg;
    private Button refresh;

    private RecyclerView recyclerView;
    private List<Model> modelList;
    private DataAdapter adapter;

    private FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.arrow_left);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();

        title = intent.getStringExtra("title");
        category = intent.getStringExtra("category");

        getSupportActionBar().setTitle(title);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Bundle params = new Bundle();
        params.putString("category_name", category);
        //firebaseAnalytics.logEvent("category", params);
        firebaseAnalytics.logEvent(title, params);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        errorImg = (ImageView) findViewById(R.id.errorImg);
        refresh = (Button) findViewById(R.id.refresh);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWallpaperFromCategory(category);
            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        recyclerView.setItemAnimator(itemAnimator);

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            GestureDetector gestureDetector = new GestureDetector(getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if(child != null && gestureDetector.onTouchEvent(e)) {
                    final int position = rv.getChildAdapterPosition(child);

                    Intent intent = new Intent(getApplicationContext(), DataPictureActivity.class);
                    intent.putExtra("id", modelList.get(position).getId());
                    intent.putExtra("name", modelList.get(position).getName());
                    intent.putExtra("tag", modelList.get(position).getTag());
                    intent.putExtra("path", modelList.get(position).getPath());
                    intent.putExtra("category", modelList.get(position).getCategory());
                    startActivity(intent);
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        getWallpaperFromCategory(category);
    }

    private void getWallpaperFromCategory(String category) {

        progressBar.setVisibility(View.VISIBLE);
        errorImg.setVisibility(View.INVISIBLE);
        refresh.setVisibility(View.INVISIBLE);

        Map<String, String> categoryMap = new HashMap<>();
        categoryMap.put("category", category);

        Query.getInstanse().getWallpaperFromCategory(categoryMap).enqueue(new Callback<List<Model>>() {
            @Override
            public void onResponse(Call<List<Model>> call, Response<List<Model>> response) {
                modelList = response.body();

                setAdapter(modelList);

                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<List<Model>> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                errorImg.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);

                FirebaseCrash.report(t);
            }
        });

        Query.getInstanse().getWallpaperFromCategory(categoryMap).cancel();

        categoryMap.clear();
    }

    private void setAdapter(List<Model> modelList) {
        adapter = new DataAdapter(modelList, getApplicationContext());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
