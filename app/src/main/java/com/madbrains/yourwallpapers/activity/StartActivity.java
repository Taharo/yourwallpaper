package com.madbrains.yourwallpapers.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.madbrains.yourwallpapers.R;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        //Fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Thread logoTimer = new Thread() {
            public void run()
            {
                try {
                    int logoTimer = 0;
                    while(logoTimer < 2000) {
                        sleep(100);
                        logoTimer = logoTimer + 100;
                    }
                }
                catch (InterruptedException e) {

                    e.printStackTrace();
                }
                finally {
                    finish(); //Закрываем текущий экран
                    startActivity(new Intent(StartActivity.this, MainActivity.class));
                }
            }
        };
        logoTimer.start();
    }
}
