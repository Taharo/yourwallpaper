package com.madbrains.yourwallpapers.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.ads.purchase.InAppPurchase;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.madbrains.yourwallpapers.R;
import com.madbrains.yourwallpapers.fragments.TabsPagerFragmentAdapter;
import com.mukesh.permissions.AppPermissions;
import com.onesignal.OneSignal;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabLayout;

    private static final String[] ALL_PERMISSIONS = {
            Manifest.permission.INTERNET,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    private static final int INTERNET_REQUEST_CODE = 1;
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 2;
    //private static final int READ_EXTERNAL_STORAGE_REQUEST_CODE = 3;

    private FirebaseAnalytics firebaseAnalytics;

    private AppPermissions runtimePermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        runtimePermission = new AppPermissions(this);

        checkPermission();

        initTabs();

        OneSignal.startInit(this).init(); //Уведомления

        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "Your Wallpapers");
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    private void initTabs() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        TabsPagerFragmentAdapter adapter = new TabsPagerFragmentAdapter(this, getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(1);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void checkPermission() {
        if (runtimePermission.hasPermission(Manifest.permission.INTERNET)) {
            Log.d("Permission", "INTERNET permission given");
        } else {
            runtimePermission.requestPermission(Manifest.permission.INTERNET, INTERNET_REQUEST_CODE);
        }

        if (runtimePermission.hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Log.d("Permission", "WRITE_EXTERNAL_STORAGE permission given");
        } else {
            runtimePermission.requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
        }

        /*if (runtimePermission.hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Log.d("Permission", "READ_EXTERNAL_STORAGE permission given");
        } else {
            runtimePermission.requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE_REQUEST_CODE);
        }*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_rate_it:
                Intent intentRate = new Intent(Intent.ACTION_VIEW);
                intentRate.setData(Uri.parse("market://details?id=com.madbrains.yourwallpapers"));
                startActivity(intentRate);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case INTERNET_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, "Permissions not granted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permissions granted", Toast.LENGTH_SHORT).show();
                }
                break;

            case WRITE_EXTERNAL_STORAGE_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, "Permissions not granted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permissions granted", Toast.LENGTH_SHORT).show();
                }
                break;
            /*case READ_EXTERNAL_STORAGE_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, "Permissions not granted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permissions granted", Toast.LENGTH_SHORT).show();
                }
                break;*/
        }
    }
}
