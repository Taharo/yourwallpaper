package com.madbrains.yourwallpapers.activity;

import android.Manifest;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;
import com.madbrains.yourwallpapers.R;
import com.madbrains.yourwallpapers.model.ModelForUpdate;
import com.madbrains.yourwallpapers.provider.Query;
import com.mukesh.permissions.AppPermissions;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataPictureActivity extends AppCompatActivity implements View.OnClickListener {

    private String id, name, tag, path, category;

    private ImageView picture;
    private ProgressBar progressBar;

    final String strPref_Download_ID = "PREF_DOWNLOAD_ID";

    private DownloadManager downloadManager;
    private SharedPreferences preferenceManager;
    private DownloadManager.Request request;

    private ModelForUpdate model;

    private static final String[] ALL_PERMISSIONS = {
            Manifest.permission.INTERNET,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    private static final int INTERNET_REQUEST_CODE = 1;
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 2;
    //private static final int READ_EXTERNAL_STORAGE_REQUEST_CODE = 3;

    private AppPermissions runtimePermission;
    
    private Boolean pictureLoaded = false;

    private FirebaseAnalytics firebaseAnalytics;

    private AdView adView;

    private final int REQUEST_CODE_FOR_SET_IMAGE = 10;
    private static final int REQUEST_CODE_FOR_SHARE = 12;

    private File f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_picture);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.arrow_left);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        runtimePermission = new AppPermissions(this);

        checkPermission();

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-6157409547045190~7061505062");

        adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        preferenceManager = PreferenceManager.getDefaultSharedPreferences(this);
        downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        
        final com.getbase.floatingactionbutton.FloatingActionButton like = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.share);
        like.setOnClickListener(this);
        final com.getbase.floatingactionbutton.FloatingActionButton download = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.download);
        download.setOnClickListener(this);
        final com.getbase.floatingactionbutton.FloatingActionButton setAsWallpaper = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.set);
        setAsWallpaper.setOnClickListener(this);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        
        Intent intent = getIntent();

        id = intent.getStringExtra("id");
        name = intent.getStringExtra("name");
        tag = intent.getStringExtra("tag");
        path = intent.getStringExtra("path");
        category = intent.getStringExtra("category");

        picture = (ImageView) findViewById(R.id.picture);

        progressBar.setVisibility(View.VISIBLE);
        Picasso.with(this)
                .load(path)
                //.error(R.drawable.sad)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(picture, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.INVISIBLE);
                        pictureLoaded = true;
                    }

                    @Override
                    public void onError() {
                        //onError
                        pictureLoaded = false;
                    }
                });

        updateViewsCount(); //Увеличиваем количество просмотров в БД на 1
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.share:
                sharePicture();
                break;
            case R.id.download:
                downloadPicture();
                break;
            case R.id.set:
                setWallpaper();
                break;
        }
    }

    private void sharePicture() {

        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "Your Wallpapers");
        if (!directory.exists()) {
            directory.mkdirs();
        }

        if (pictureLoaded) {
            final Thread thread = new Thread() {
                public void run() {
                    Bitmap inImage = ((BitmapDrawable) picture.getDrawable()).getBitmap();

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                    f = new File(Environment.getExternalStorageDirectory() + File.separator + name + ".jpg");

                    boolean created = false;
                    try {
                        created = f.createNewFile();
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        FirebaseCrash.report(e);
                    }

                    Uri uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + File.separator + name + ".jpg"));

                    Intent share = new Intent(Intent.ACTION_SEND);

                    share.setType("image/*");

                    share.putExtra(Intent.EXTRA_STREAM, uri);

                    startActivityForResult(Intent.createChooser(share, getString(R.string.share_to)), REQUEST_CODE_FOR_SHARE);
                }

            };
            thread.start();

        } else {
            Toast.makeText(DataPictureActivity.this, R.string.wait, Toast.LENGTH_SHORT).show();
        }

        Bundle params = new Bundle();
        params.putString("image_name", name);
        firebaseAnalytics.logEvent("share_image", params);
    }

    private void setWallpaper() {

        if (pictureLoaded) {
            Thread thread = new Thread() {
                public void run() {
                    Bitmap inImage = ((BitmapDrawable) picture.getDrawable()).getBitmap();

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                    f = new File(Environment.getExternalStorageDirectory() + File.separator + name + ".jpg");

                    boolean created = false;

                    try {
                        created = f.createNewFile();
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(bytes.toByteArray());
                        fo.close();

                        Uri uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + File.separator + name + ".jpg"));

                        Intent intent = new Intent(Intent.ACTION_ATTACH_DATA);
                        intent.setDataAndType(uri, "image/*");
                        intent.putExtra("jpg", "image/*");

                        startActivityForResult(Intent.createChooser(intent, getString(R.string.set_as)), REQUEST_CODE_FOR_SET_IMAGE);

                    } catch (IOException e) {
                        e.printStackTrace();
                        FirebaseCrash.report(e);
                    }
                }

            };
            thread.start();
        } else {
            Toast.makeText(DataPictureActivity.this, R.string.wait, Toast.LENGTH_SHORT).show();
        }

        Bundle params = new Bundle();
        params.putString("image_name", name);
        firebaseAnalytics.logEvent("set_image", params);
    }

    private void downloadPicture() {
        if (pictureLoaded) {
            try {
                File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "Your Wallpapers");
                if (!directory.exists()) {
                    directory.mkdirs();
                }

                request = new DownloadManager.Request(Uri.parse(path));
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                request.setDestinationInExternalPublicDir("/Your Wallpapers", name + ".jpg");

                long id = downloadManager.enqueue(request);

                // Save the request id
                SharedPreferences.Editor PrefEdit = preferenceManager.edit();
                PrefEdit.putLong(strPref_Download_ID, id);
                PrefEdit.apply();
            } catch (Exception e){
                e.printStackTrace();
                FirebaseCrash.report(e);
            }
        } else {
            Toast.makeText(DataPictureActivity.this, R.string.wait, Toast.LENGTH_SHORT).show();
        }

        Bundle params = new Bundle();
        params.putString("image_name", name);
        firebaseAnalytics.logEvent("download_image", params);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(downloadReceiver);

        adView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(downloadReceiver, intentFilter);

        adView.resume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        adView.destroy();
    }

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context arg0, Intent arg1) {

            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(preferenceManager.getLong(strPref_Download_ID, 0));
            Cursor cursor = downloadManager.query(query);
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                int status = cursor.getInt(columnIndex);
                if (status == DownloadManager.STATUS_SUCCESSFUL) {
                    Toast.makeText(DataPictureActivity.this, R.string.successfully, Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_data_picture, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_set_as_wallpaper:
                setWallpaper();
                break;
            case R.id.action_download:
                downloadPicture();
                break;
            case R.id.action_share:
                sharePicture();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateViewsCount() {

        Map<String, String> pictureName = new HashMap<>();
        pictureName.put("name", name);

        Query.getInstanse().updateViewsCount(pictureName).enqueue(new Callback<ModelForUpdate>() {
            @Override
            public void onResponse(Call<ModelForUpdate> call, Response<ModelForUpdate> response) {
                model = response.body();
                String status = model.getStatus(); //OK
                //Доделать что бы при ошибке делало повторный запрос
            }

            @Override
            public void onFailure(Call<ModelForUpdate> call, Throwable t) {
                FirebaseCrash.report(t);
            }
        });

        Query.getInstanse().updateViewsCount(pictureName).cancel();
        pictureName.clear();
    }

    private void checkPermission() {
        if (runtimePermission.hasPermission(Manifest.permission.INTERNET)) {
            Log.d("Permission", "INTERNET permission given");
        } else {
            runtimePermission.requestPermission(Manifest.permission.INTERNET, INTERNET_REQUEST_CODE);
        }

        if (runtimePermission.hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Log.d("Permission", "WRITE_EXTERNAL_STORAGE permission given");
        } else {
            runtimePermission.requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
        }

        /*if (runtimePermission.hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Log.d("Permission", "READ_EXTERNAL_STORAGE permission given");
        } else {
            runtimePermission.requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE_REQUEST_CODE);
        }*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        boolean deleted = false;

        switch(requestCode) {
            case REQUEST_CODE_FOR_SET_IMAGE:
                if(resultCode == RESULT_OK) {
                    Toast.makeText(DataPictureActivity.this, R.string.updated, Toast.LENGTH_SHORT).show();
                }
                deleted = f.delete();
                break;
            case REQUEST_CODE_FOR_SHARE:
                deleted = f.delete(); //Удаляем файл после того как поделились им
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case INTERNET_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, "Permissions not granted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permissions granted", Toast.LENGTH_SHORT).show();
                }
                break;

            case WRITE_EXTERNAL_STORAGE_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, "Permissions not granted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permissions granted", Toast.LENGTH_SHORT).show();
                }
                break;
            /*case READ_EXTERNAL_STORAGE_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, "Permissions not granted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permissions granted", Toast.LENGTH_SHORT).show();
                }
                break;*/
        }
    }
}
