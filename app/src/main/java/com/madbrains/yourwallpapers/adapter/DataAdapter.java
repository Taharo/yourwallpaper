package com.madbrains.yourwallpapers.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.madbrains.yourwallpapers.CropSquareTransformation;
import com.madbrains.yourwallpapers.R;
import com.madbrains.yourwallpapers.model.Model;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;


public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private List<Model> modelList;
    private Context context;

    public DataAdapter(List<Model> modelList, Context context) {
        this.modelList = modelList;
        this.context = context;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.tab_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DataAdapter.ViewHolder holder, int position) {

        Model model = modelList.get(position);

        CropSquareTransformation transform = new CropSquareTransformation();

        holder.progressBar.setVisibility(View.VISIBLE);

        Picasso.with(context)
                .load(model.getPath())
                //.placeholder(R.drawable.placeholder)
                //.error(R.drawable.sad)
                .resize(450, 450)
                .transform(transform)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(holder.picture, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView picture;
        ProgressBar progressBar;
        public ViewHolder(View view) {
            super(view);
            picture = (ImageView) view.findViewById(R.id.picture);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        }
    }
}
