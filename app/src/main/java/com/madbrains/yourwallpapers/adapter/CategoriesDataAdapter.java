package com.madbrains.yourwallpapers.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.madbrains.yourwallpapers.R;

import java.util.ArrayList;


public class CategoriesDataAdapter extends RecyclerView.Adapter<CategoriesDataAdapter.ViewHolder> {

    private ArrayList<String> categories;
    private ArrayList<Integer> logos;
    private Context context;

    public CategoriesDataAdapter(ArrayList<String> categories, ArrayList<Integer> logos, Context context) {
        this.categories = categories;
        this.logos = logos;
        this.context = context;
    }

    @Override
    public CategoriesDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoriesDataAdapter.ViewHolder holder, int position) {
            holder.categoryName.setText(categories.get(position));
            holder.logo.setImageResource(logos.get(position));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView categoryName;
        private ImageView logo;
        public ViewHolder(View view) {
            super(view);
            categoryName = (TextView) view.findViewById(R.id.categoryName);
            logo = (ImageView) view.findViewById(R.id.logo);
        }
    }
}
