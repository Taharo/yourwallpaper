package com.madbrains.yourwallpapers.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.madbrains.yourwallpapers.R;
import com.madbrains.yourwallpapers.activity.CategoryActivity;
import com.madbrains.yourwallpapers.adapter.CategoriesDataAdapter;
import com.madbrains.yourwallpapers.adapter.DividerItemDecoration;

import java.util.ArrayList;

public class Categories extends AbstractTabFragment {

    private static final int LAYOUT = R.layout.categories_layout;

    private RecyclerView recyclerView;
    private CategoriesDataAdapter adapter;

    private ArrayList<String> categories;
    private ArrayList<Integer> logos;

    public static Categories getInstance(Context context) {
        Bundle args = new Bundle();
        Categories fragment = new Categories();
        fragment.setArguments(args);
        fragment.setContext(context);
        fragment.setTitle(context.getString(R.string.tab1));

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(LAYOUT, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        recyclerView.setItemAnimator(itemAnimator);

        categories = new ArrayList<>();
        logos = new ArrayList<>();

        categories.clear();
        logos.clear();

        categories.add(getActivity().getApplicationContext().getString(R.string.category_abstract));
        categories.add(getActivity().getApplicationContext().getString(R.string.category_animals));
        categories.add(getActivity().getApplicationContext().getString(R.string.category_art));
        categories.add(getActivity().getApplicationContext().getString(R.string.category_cars));
        categories.add(getActivity().getApplicationContext().getString(R.string.category_food));
        categories.add(getActivity().getApplicationContext().getString(R.string.category_games));
        categories.add(getActivity().getApplicationContext().getString(R.string.category_nature));
        categories.add(getActivity().getApplicationContext().getString(R.string.category_places));

        logos.add(R.drawable.logo_abstract);
        logos.add(R.drawable.logo_animals);
        logos.add(R.drawable.logo_art);
        logos.add(R.drawable.logo_cars);
        logos.add(R.drawable.logo_food);
        logos.add(R.drawable.logo_games);
        logos.add(R.drawable.logo_nature);
        logos.add(R.drawable.logo_places);

        adapter = new CategoriesDataAdapter(categories, logos, getActivity().getApplicationContext());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            GestureDetector gestureDetector = new GestureDetector(getActivity().getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if(child != null && gestureDetector.onTouchEvent(e)) {
                    final int position = rv.getChildAdapterPosition(child);

                    switch (position){
                        case 0:
                            chooseCategory("abstract", categories.get(position));
                            break;
                        case 1:
                            chooseCategory("animals", categories.get(position));
                            break;
                        case 2:
                            chooseCategory("art", categories.get(position));
                            break;
                        case 3:
                            chooseCategory("cars", categories.get(position));
                            break;
                        case 4:
                            chooseCategory("food", categories.get(position));
                            break;
                        case 5:
                            chooseCategory("games", categories.get(position));
                            break;
                        case 6:
                            chooseCategory("nature", categories.get(position));
                            break;
                        case 7:
                            chooseCategory("places", categories.get(position));
                            break;
                    }
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        return view;
    }

    private void chooseCategory(String category, String title){
        Intent intent = new Intent(getActivity().getApplicationContext(), CategoryActivity.class);
        intent.putExtra("category", category);
        intent.putExtra("title", title);
        startActivity(intent);
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
