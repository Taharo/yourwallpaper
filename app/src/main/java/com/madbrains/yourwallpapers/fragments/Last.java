package com.madbrains.yourwallpapers.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.madbrains.yourwallpapers.R;
import com.madbrains.yourwallpapers.activity.DataPictureActivity;
import com.madbrains.yourwallpapers.adapter.DataAdapter;
import com.madbrains.yourwallpapers.model.Model;
import com.madbrains.yourwallpapers.provider.Query;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Last extends AbstractTabFragment {

    private static final int LAYOUT = R.layout.tab_layout;

    private ProgressBar progressBar;
    private ImageView errorImg;
    private Button refresh;

    private RecyclerView recyclerView;
    private List<Model> modelList;
    private DataAdapter adapter;

    private boolean loaded;

    public static Last getInstance(Context context) {
        Bundle args = new Bundle();
        Last fragment = new Last();
        fragment.setArguments(args);
        fragment.setContext(context);
        fragment.setTitle(context.getString(R.string.tab2));

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loaded = false;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(LAYOUT, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        errorImg = (ImageView) view.findViewById(R.id.errorImg);
        refresh = (Button) view.findViewById(R.id.refresh);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLastWallpapper();
            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        recyclerView.setItemAnimator(itemAnimator);

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            GestureDetector gestureDetector = new GestureDetector(getActivity().getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if(child != null && gestureDetector.onTouchEvent(e)) {
                    final int position = rv.getChildAdapterPosition(child);

                    Intent intent = new Intent(getActivity().getApplicationContext(), DataPictureActivity.class);
                    intent.putExtra("id", modelList.get(position).getId());
                    intent.putExtra("name", modelList.get(position).getName());
                    intent.putExtra("tag", modelList.get(position).getTag());
                    intent.putExtra("path", modelList.get(position).getPath());
                    intent.putExtra("category", modelList.get(position).getCategory());
                    startActivity(intent);
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        if (!loaded) {
            getLastWallpapper();
        }  else {
            setAdapter(modelList);
        }

        return view;
    }

    private void getLastWallpapper() {

        progressBar.setVisibility(View.VISIBLE);
        errorImg.setVisibility(View.INVISIBLE);
        refresh.setVisibility(View.INVISIBLE);

        Query.getInstanse().getLastWallpapper().enqueue(new Callback<List<Model>>() {
            @Override
            public void onResponse(Call<List<Model>> call, Response<List<Model>> response) {
                modelList = response.body();

                setAdapter(modelList);

                progressBar.setVisibility(View.INVISIBLE);

                loaded = true;
            }

            @Override
            public void onFailure(Call<List<Model>> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                errorImg.setVisibility(View.VISIBLE);
                refresh.setVisibility(View.VISIBLE);
                FirebaseCrash.report(t);

                loaded = false;
            }
        });

        Query.getInstanse().getLastWallpapper().cancel();
    }

    private void setAdapter(List<Model> modelList) {
        adapter = new DataAdapter(modelList, getActivity().getApplicationContext());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void setContext(Context context) {
        this.context = context;
    }

}
