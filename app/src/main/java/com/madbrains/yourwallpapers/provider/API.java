package com.madbrains.yourwallpapers.provider;

import com.madbrains.yourwallpapers.model.Model;
import com.madbrains.yourwallpapers.model.ModelForUpdate;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface API  {
    @GET("last_wallpaper.php")
    Call<List<Model>> getLastWallpapper();

    @FormUrlEncoded
    @POST("category_wallpaper.php")
    Call<List<Model>> getWallpaperFromCategory(@FieldMap Map<String, String> category);

    @FormUrlEncoded
    @POST("updateviews.php")
    Call<ModelForUpdate> updateViewsCount(@FieldMap Map<String, String> name);

    @GET("popularwallpaper.php")
    Call<List<Model>> getPopularWallpapper();
}
