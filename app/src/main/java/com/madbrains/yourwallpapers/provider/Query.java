package com.madbrains.yourwallpapers.provider;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Query {

    private static API instanse;

    public static API getInstanse() {
        if(instanse == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://taharo.pp.ua/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            instanse = retrofit.create(API.class);
            return instanse;
        }

        return instanse;
    }


}
